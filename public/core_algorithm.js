
var dict_set = new Set();
var full_len = dict.length;

for (var i = 0; i<full_len; ++i)
{
    dict_set.add(dict[i]);
}

function show(dict_set) {
	if (dict_set.size == 0) {
		alert("Dictionary is too small or not found.");
		return;
	}
	var visited_set = new Set();
	var first = document.getElementById("first").value;
	var second = document.getElementById("last").value;
	var begin_end = document.getElementById("begin_end_exclusion").checked;
	var diff_length = document.getElementById("diff_length_allowed").checked;
	first = first.toLowerCase();
	second = second.toLowerCase();
	if (!begin_end) // to check whether the two words are in the dictionary or not
	{
		if (dict_set.has(first) == false) {
			alert(first + " is not found in the dictionary. Please try again.");
			return;
		}
		if (dict_set.has(second) == false) {
			alert(second + " is not found in the dictionary. Please try again.");
			return;
		}
	}
	if (!diff_length) {
		if (first.length != second.length) {
			alert("The two words must be of different lengths. Please try again.");
			return;
		}
	}
	if (first == second) {
		alert("The two words must be different.");
		return;
	}
	var date1 = new Date();
	var answer = word_ladder(first, second, dict_set, visited_set, diff_length);
	var date2 = new Date();
	var ElapsedTime = date2.getTime() - date1.getTime();
	if (answer == '') {
		alert("Ladder not found" + '. Elapsed Time: ' + ElapsedTime + ' ms');
		return;
	}
	else {
		answer = answer.substr(0, answer.length - 1);
		var output = 'A ladder from ' + first + ' to ' + second + ': ';
		output += answer;
		alert(output + '. \nElapsed Time: ' + ElapsedTime + ' ms');
		return;
	}
}

function word_ladder(first, second, dict_set, visited_set, diff_length) { // return the ladder when it finds the ladder, otherwise return empty string
	visited_set.add(first);
	var ladder = new Array(); // "ladder" is a stack-like array. Interface: push, pop, ladder[length-1] (the top of the stack)
	var search = new Array(); // "search" is a queue-like array. Interface: push, shift, search[0] (the front of the queue)
	ladder.push(first);
	search.push(ladder);
	while (search.length != 0) {
		var partial_ladder = search.shift();
		while (true) {
			var neighbour = find_neighbour(partial_ladder[partial_ladder.length - 1], dict_set, visited_set, diff_length);
			if (neighbour == '') {
				break;
			}
			if (neighbour == second) {
				partial_ladder.push(neighbour);
				var output = '';
				var len = partial_ladder.length;
				for (var i = 0; i < len ; ++i) {
					output = output + partial_ladder[i] + ' ';
				}
				return output;
			}
			else {
				var partial_ladder_copy = partial_ladder.slice(0);
				partial_ladder_copy.push(neighbour);
				search.push(partial_ladder_copy);
			}
		}
	}
	return '';
}

function find_neighbour(former, dict_set, visited_set, diff_length) { // return its neighbour when it succeed, otherwise return empty string
	// to find the neighbor word with only one different letter
	var its_neighbour = former;
	for (var i = 0; i < its_neighbour.length; i++) // i: position in the word
	{
		its_neighbour = former;
		for (var j = 0; j < 26; j++) // j: iterator for alphabets
		{
			var subs = String.fromCharCode(97 + j); // subs: a to z
			its_neighbour = changeStr(its_neighbour, i, subs);
			if (check_neighbour(former,its_neighbour,dict_set,visited_set)) {
				visited_set.add(its_neighbour);
				return its_neighbour;
			}
		}
	}

	if (diff_length) {
		// to find the neighbor word with one less letter
		its_neighbour = former;
		for (var i = 0; i < its_neighbour.length; i++) {
			its_neighbour = former;
			its_neighbour = its_neighbour.substring(0, i) + its_neighbour.substring(i + 1);
			if (check_neighbour(former,its_neighbour,dict_set,visited_set)) {
				visited_set.add(its_neighbour);
				return its_neighbour;
			}
		}

		// to find the neighbor word with one more letter
		its_neighbour = former;
		for (var i = 0; i <= its_neighbour.length; i++) {
			its_neighbour = former;
			for (var j = 0; j < 26; j++) {
				var subs = String.fromCharCode(97 + j);
				its_neighbour = its_neighbour.substring(0, i) + subs + its_neighbour.substring(i);
				if (check_neighbour(former,its_neighbour,dict_set,visited_set)) {
					visited_set.add(its_neighbour);
					return its_neighbour;
				}
				else {
					its_neighbour = former; // remember to restore the neighbour word!
				}
			}
		}
	}
	return '';
}

function changeStr(formerstr, pos, newstr) // Change a string at specific pos, return empty string when pos is invalid
{
	if (pos < 0) return '';
	if (pos >= formerstr.length) return '';
	return formerstr.substring(0, pos) + newstr + formerstr.substring(pos + 1);
}

function check_neighbour(former,its_neighbour,dict_set,visited_set) // to check if a neighbour is valid
{
	return ((its_neighbour != former) && (dict_set.has(its_neighbour) == true) && (visited_set.has(its_neighbour) == false));
}